# TV AI NEWS Frontend service
Tv-ai-news is a front end application built in Angular 6 that creates and automatically, AI generated news channel.
The platform reads a list of video news produced by the engine and served by the API backend. 
For each video there is the possibility to express a VOTE on the video, if it is related to the news description displayed.
Also there is the possibility to view stats of data present in the database.
Finally there is the possibility to custom add sources (the backend doesn't yet register the new information).

# Prerequisites

- Docker
- Angular CLI
- Node backend running (https://bitbucket.org/andrea-f/tv-ai-news-backend/src)

# How to run
- Cd in folder where there is `package.json`
- `npm install`
- `ng serve --open`
- Output can be seen in `http://localhost:4200/`
- To build project: `ng build --prod`

# How to test
- Cd in root folder
- `ng test --source-map=false`, the `source_map` flag makes sure that no HTTP requests are actually performed during testing.

The TV-AI-NEWS app is currently hosted on: http://tv-ai-news.radiolondra.co.uk