FROM nginx:alpine
COPY nginx.conf /etc/nginx/conf.d/default.conf
WORKDIR /usr/share/nginx/html
COPY dist/tv-ai-news/ .
COPY src/assets/config_parameters_prod.json /usr/share/nginx/html/assets/config_parameters.json
