import { TestBed, inject } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { SourcesService } from './sources.service';
import { ConfigService } from './config.service';
import { defer } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';

let httpClientSpy: { get: jasmine.Spy, put: jasmine.Spy, post: jasmine.Spy };

let sourcesService: SourcesService;
let configService: ConfigService;

export function asyncData<T>(data: T) {
  return defer(() => Promise.resolve(data));
}

describe('SourcesService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put']);
    TestBed.configureTestingModule({
      providers: [
      SourcesService,
      ConfigService,
      CookieService,
      {provide: HttpClient, useValue: httpClientSpy}
      ]
    });
    configService = TestBed.get(ConfigService);
    spyOn(configService, 'getConfig').and.returnValue({
      'sourcesUrl': 'empty'
    });
    sourcesService = new SourcesService(<any> httpClientSpy, <any> configService);
  });

  it('should be created', inject([SourcesService], (service: SourcesService) => {
    expect(service).toBeTruthy();
  }));

  it('should call getSources with GET and return object', inject([SourcesService], (service: SourcesService) => {
    const expectedData: object = {};
    httpClientSpy.get.and.returnValue(asyncData(expectedData));
    sourcesService.getSources().subscribe(
      data => expect(data).toBeTruthy(),
      fail
    );
    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  }));

  it('should call editSource with PUT and return object', inject([SourcesService], (service: SourcesService) => {
    const expectedData: object = {};
    httpClientSpy.put.and.returnValue(asyncData(expectedData));
    sourcesService.editSource(expectedData).subscribe(
      data => expect(data).toBeTruthy(),
      fail
    );
    expect(httpClientSpy.put.calls.count()).toBe(1, 'one call');
  }));

});
