import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {

  configData;
  userIdentifier;
  videoId;

  constructor(
    private http: HttpClient,
    private configService: ConfigService
  ) {
    this.configData = configService.getConfig();
    this.userIdentifier = configService.getIdentifier();
  }

  add(body): Observable<any> {
    // Send a comment to the backend
    body.cookie = this.userIdentifier;
    return this.http.post(this.configData.addCommentUrl, body);
  }
}
