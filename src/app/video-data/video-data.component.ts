import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Video } from '../video';
import { VideoService } from '../video.service';

@Component({
  selector: 'app-video-data',
  templateUrl: './video-data.component.html',
  styleUrls: ['./video-data.component.css']
})
export class VideoDataComponent implements OnInit, OnDestroy {

  @Input() video: Video;

  totalScore: number;
  voted: boolean;
  votedService;

  constructor(
    private videoService: VideoService,
  ) {
  }

  ngOnInit() {
    if (typeof this.video.votes !== 'undefined') {
    if (this.video.votes.length === 0) {
      this.totalScore = 0;
    } else {
      this.countVotes();
    }}
  }

  countVotes() {
    // Updates the total score variable
    this.totalScore = 0;
    this.video.votes.forEach((item) => {
      this.totalScore += item;
    });
  }

  vote(preference: Number) {
    this.video.votes.push(preference);
    this.votedService = this.videoService.vote(this.video).subscribe((resp) => {
      this.voted = true;
    });
    this.countVotes();
  }

  ngOnDestroy() {
    if (this.votedService !== undefined) {
      this.votedService.unsubscribe();
    }
  }
}
