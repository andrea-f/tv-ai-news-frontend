import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { VideoDataComponent } from './video-data.component';
import { Video } from '../video';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config.service';
import { VideoService } from '../video.service';
import { of } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';

let httpClientSpy: { get: jasmine.Spy, post: jasmine.Spy  };

let configService: ConfigService;
let videoService: VideoService;

describe('VideoDataComponent', () => {
  let component: VideoDataComponent;
  let fixture: ComponentFixture<VideoDataComponent>;
  let video: Video;

  beforeEach(async(() => {

    video = new Video({
      'votes': [],
      'video_id': 'abc',
      'data': {}
    });

    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post']);
    TestBed.configureTestingModule({
      declarations: [ VideoDataComponent ],
      providers: [
        ConfigService,
        CookieService,
        {provide: HttpClient, useValue: httpClientSpy},
        VideoService,
      ]
    })
    .compileComponents();
    configService = TestBed.get(ConfigService);
    videoService = TestBed.get(VideoService);
    spyOn(configService, 'getConfig').and.returnValue({
      'sourcesUrl': 'empty',
      'baseUrl': 'empty'
    });
    spyOn(videoService, 'vote').and.returnValue(of({
      'voted': 'successfully'
    }));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoDataComponent);
    component = fixture.componentInstance;
    component.video = video;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('can vote', async(() => {
    fixture.detectChanges();
    component.video = new Video({
      'votes': [],
      'video_id': 'awbc',
      'data': {}
    });
    component.vote(1);
    expect(videoService.vote).toHaveBeenCalled();
    expect(component).toBeTruthy();
    expect(component.totalScore).toBe(1);
  }));
});
