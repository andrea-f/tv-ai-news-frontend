import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config.service';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class SourcesService {

  configData;
  // Holds sources as instance of Source
  // and sourcesNameList
  data;
  router;

  constructor(
    private http: HttpClient,
    private configService: ConfigService
    ) {
    this.configData = configService.getConfig();
  }

  editSource(body): Observable<any> {
    // Does a PUT request to update a value
    return this.http.put(this.configData.sourcesUrl, body);
  }

  getSources(): Observable<any> {
    return this.http.get(this.configData.sourcesUrl);
  }
}
