import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ConfigService } from '../config.service';
import { TrendingComponent } from './trending.component';
import { TrendingService } from '../trending.service';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { of } from 'rxjs';

let httpClientSpy: { get: jasmine.Spy, put: jasmine.Spy };
let configService: ConfigService;

describe('TrendingComponent', () => {
  let component: TrendingComponent;
  let fixture: ComponentFixture<TrendingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrendingComponent ],
      providers: [
        ConfigService,
        TrendingService,
        CookieService,
        {provide: HttpClient, useValue: httpClientSpy}
      ],
    })
    .compileComponents();

    const expectedData: object = {
        country: 'test',
        stats: {most_used_terms: [{
            count: 1,
            _id: 'aWord'
        }]}
    };
    const trendingService = TestBed.get(TrendingService);
    spyOn(trendingService, 'getTrends').and.returnValue(of(expectedData));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrendingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should have as 'aWord' as word in trending list`, async(() => {
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('li').textContent).toContain('aWord');
  }));


});
