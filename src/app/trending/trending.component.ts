import { Component, OnInit, OnDestroy } from '@angular/core';
import { TrendingService } from '../trending.service';
import { Source } from '../source';

@Component({
  selector: 'app-trending',
  templateUrl: './trending.component.html',
  styleUrls: ['./trending.component.css']
})
export class TrendingComponent implements OnInit, OnDestroy {

  source: Source;
  trendsGet;
  params;
  data;

  constructor(
    private trendingService: TrendingService,
  ) { }

  ngOnInit() {
    const body = this.trendingService.data;
    this.trendsGet = this.trendingService.getTrends(body).subscribe(data => {
      // Load the retrieved source in a Source class
      console.log(data);
      if (data) {
        this.source = new Source(data);
      }
    });
  }

  ngOnDestroy() {
    if (this.trendsGet) {
      this.trendsGet.unsubscribe();
    }
  }

}
