import { TestBed, inject} from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config.service';
import { VideoService } from './video.service';
import { defer } from 'rxjs';
import { Video } from './video';
import { CookieService } from 'ngx-cookie-service';


let httpClientSpy: { get: jasmine.Spy, post: jasmine.Spy  };
let videoService: VideoService;
let configService: ConfigService;

export function asyncData<T>(data: T) {
  return defer(() => Promise.resolve(data));
}

describe('VideoService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post']);
    TestBed.configureTestingModule({
      providers: [
      VideoService,
      ConfigService,
      CookieService,
      {provide: HttpClient, useValue: httpClientSpy}
      ]
    });
    configService = TestBed.get(ConfigService);
    spyOn(configService, 'getConfig').and.returnValue({'baseUrl': 'empty'});
    videoService = new VideoService(<any> httpClientSpy, <any> configService);
  });

  it('should be created', () => {
    expect(videoService).toBeTruthy();
  });

  it('should call vote with POST and return ok', () => {
    const expectedData: object = {};
    httpClientSpy.post.and.returnValue(asyncData(expectedData));
    videoService.vote(new Video({
      'data': {},
      'votes': [],
      'video_id': 'abc'
    })).subscribe(
      data => expect(data).toBeTruthy(),
      fail
    );
    expect(httpClientSpy.post.calls.count()).toBe(1, 'one call');
  });

  it('should return expected videos (HttpClient called once)', () => {
    const expectedData: object = {};
    httpClientSpy.get.and.returnValue(asyncData(expectedData));
    videoService.getLatestNews(expectedData).subscribe(
      data => expect(data).toBeTruthy(),
      fail
    );
    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  });

});
