import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { HttpClient } from '@angular/common/http';
import { VideoService } from './video.service';
import { VideoPlayerComponent } from './video-player/video-player.component';
import { SourcesComponent } from './sources/sources.component';
import { AddSourcesComponent } from './add-sources/add-sources.component';
import { Video } from './video';
import { YoutubePlayerModule } from 'ngx-youtube-player';
import { VideoDataComponent } from './video-data/video-data.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddSourcesDetailComponent } from './add-sources-detail/add-sources-detail.component';
import { defer, of } from 'rxjs';
import { ConfigService } from './config.service';
import { RouterTestingModule, tick } from '@angular/router/testing';
import { TrendingComponent } from './trending/trending.component';
import { CommentsComponent } from './comments/comments.component';
import { CookieService } from 'ngx-cookie-service';
import { Router, RoutesRecognized, RouterStateSnapshot } from '@angular/router';

let httpClientSpy: { get: jasmine.Spy };
let configService: ConfigService;
let videoService: VideoService;
let videoServiceSpy;
export function asyncData<T>(data: T) {
  return defer(() => Promise.resolve(data));
}

const expectedData = {
  news: [{video_id: 'xyz', source: 'cnn', description: 'abcd', data: {}, votes: {}}],
  sources: []
};

const createRouterStateSnapshot = function () {
    const routerStateSnapshot = jasmine.createSpyObj('RouterStateSnapshot',
                                                     ['toString', 'root']);
    routerStateSnapshot.root = jasmine.createSpyObj('root', ['firstChild']);
    routerStateSnapshot.root.firstChild.params = {
        country: 'uk',
        source: 'bbc'
    };
    return <RouterStateSnapshot>routerStateSnapshot;
};

class MockRouter {
    public events = of(new RoutesRecognized(2, '/', '/', createRouterStateSnapshot()));
}

describe('AppComponent', () => {
  beforeEach(async(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post']);
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        VideoPlayerComponent,
        SourcesComponent,
        AddSourcesComponent,
        VideoDataComponent,
        AddSourcesDetailComponent,
        CommentsComponent,
        TrendingComponent
      ],
      imports: [
        YoutubePlayerModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule
      ],
      providers: [
        ConfigService,
        {provide: HttpClient, useValue: httpClientSpy},
        VideoService,
        {provide: Router, useClass: MockRouter},
        CookieService
      ]
    }).compileComponents();
    configService = TestBed.get(ConfigService);
    videoService = TestBed.get(VideoService);
    spyOn(configService, 'getConfig').and.returnValue({
      'sourcesUrl': 'empty',
      'baseUrl': 'empty'
    });
    videoService.configData = {
      'sourcesUrl': 'empty',
      'baseUrl': 'empty'
    };
    videoServiceSpy = spyOn(videoService, 'getLatestNews').and.callThrough();
    httpClientSpy.get.and.returnValue(asyncData(expectedData));
  }));

  it('should create the app', async(() => {
    // videoServiceSpy.and.returnValue(asyncData(expectedData));
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'NEWS TV'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('NEWS TV');
  }));

  xit('should render title in a h3 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.whenStable().then(data => {
      // fixture.componentInstance.currentVideo = true;
      fixture.detectChanges();
      const compiled = fixture.debugElement.nativeElement;
      expect(compiled.querySelector('h3').textContent).toContain('TV AI NEWS - Artificial Intelligence News Generation platform');
    });
  }));

  it(`should call getLatestNews at least once.`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    expect(videoServiceSpy.calls.count()).toBe(1, 'one call');
  }));

  it(`should add COUNTRY and SOURCE query parameter from url in getLatestNews call.`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    expect(videoServiceSpy.calls.mostRecent().args[0]).toEqual({
      country: 'uk',
      source: 'bbc'
    });
  }));


});
