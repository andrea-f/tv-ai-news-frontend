import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TrendingService {

  configData;
  data;

  constructor(
    private http: HttpClient,
    private configService: ConfigService
  ) {
    this.configData = configService.getConfig();
  }

  getTrends(body): Observable<any> {
    return this.http.post(this.configData.getTrendsUrl, body);
  }
}
