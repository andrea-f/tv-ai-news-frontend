import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { VideoPlayerComponent } from './video-player.component';
import { YoutubePlayerModule } from 'ngx-youtube-player';
import { VideoDataComponent } from '../video-data/video-data.component';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config.service';
import { Video } from '../video';
import { CookieService } from 'ngx-cookie-service';
import { TrendingComponent } from '../trending/trending.component';

let httpClientSpy: { get: jasmine.Spy };

let configService: ConfigService;

describe('VideoPlayerComponent', () => {
  let component: VideoPlayerComponent;
  let fixture: ComponentFixture<VideoPlayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        VideoPlayerComponent,
        VideoDataComponent,
        TrendingComponent
      ],
      providers: [
        ConfigService,
        CookieService,
        {provide: HttpClient, useValue: httpClientSpy}
      ],
      imports: [
        YoutubePlayerModule
      ]
    })
    .compileComponents();
    configService = TestBed.get(ConfigService);
    spyOn(configService, 'getConfig').and.returnValue({
      'sourcesUrl': 'empty',
      'baseUrl': 'empty'
    });
  }));

  it('should create', () => {
    fixture = TestBed.createComponent(VideoPlayerComponent);
    component = fixture.componentInstance;
    component.video = new Video({
      'video_id': 'xyz',
      'data': {}
    });
    expect(component).toBeTruthy();
  });
});
