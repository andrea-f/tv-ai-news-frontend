import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Video } from '../video';
import { VideoDataComponent} from '../video-data/video-data.component';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.css']
})
export class VideoPlayerComponent {

    @Input() video: Video;
    @Input() news: string;
    @Output() playerEvent = new EventEmitter();
    @ViewChild(VideoDataComponent) videoData: VideoDataComponent;

    player: YT.Player;

    savePlayer(player) {
      this.player = player;
      this.player.playVideo();
    }

    onStateChange(event) {

      switch (event.data) {
        // Signal ended event
        case 0:
          this.playerEvent.emit();
          break;
        case -1:
          this.player.playVideo();
          // Signal loaded event and count
          this.videoData.countVotes();
          break;
      }
    }

    nextVideo() {
      this.playerEvent.emit();
      this.videoData.voted = false;
    }
}
