import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class ConfigService {
    parameters: object;
    configUrl = 'assets/config_parameters.json';
    cookieName = 'identifier';
    cookieValue: string;

    constructor(
      private http: HttpClient,
      private cookieService: CookieService
      ) {
    }

    public getConfig() {
      return this.parameters;
    }

    public getIdentifier() {
      // Reads a cookie value for the identifier, otherwise it sets it.
      const cookieExists: boolean = this.cookieService.check(this.cookieName);
      if (cookieExists) {
        return this.cookieService.get(this.cookieName);
      } else {
        const newIdentifier = this.guidGenerator();
        this.cookieService.set( this.cookieName, newIdentifier );
        return newIdentifier;
      }
    }

    private guidGenerator() {
      // Generates a new UID
      const randId = function() {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
      };
      return (randId() + randId() + '-' + randId() + '-' + randId() + '-' + randId() + '-' + randId() + randId() + randId());
    }

    load(): Promise<any> {
      return this.http.get(this.configUrl)
        .toPromise()
        .then(res => this.parameters = res)
        .catch((err) => {
          console.log(err);
        });
    }
}
