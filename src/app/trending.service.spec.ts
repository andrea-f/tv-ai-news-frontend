import { TestBed, inject } from '@angular/core/testing';
import { TrendingService } from './trending.service';
import { TestBed, inject } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config.service';
import { defer } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';

let httpClientSpy: { post: jasmine.Spy };
let configService: ConfigService;

export function asyncData<T>(data: T) {
  return defer(() => Promise.resolve(data));
}

describe('TrendingService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['post']);

    TestBed.configureTestingModule({
      providers: [
        TrendingService,
        CookieService,
        ConfigService,
        {provide: HttpClient, useValue: httpClientSpy}
      ]
    });
    configService = TestBed.get(ConfigService);
    spyOn(configService, 'getConfig').and.returnValue({
      'getTrendsUrl': 'empty'
    });
  });

  it('should be created', inject([TrendingService], (service: TrendingService) => {
    expect(service).toBeTruthy();
  }));

  it('should call getTrends with POST and return source', inject([TrendingService], (service: TrendingService) => {
    const expectedData: object = {
        stats: {most_used_terms: [{
            count: 1,
            _id: 'aWord'
        }]}
    };
    httpClientSpy.post.and.returnValue(asyncData(expectedData));
    service.getTrends().subscribe(
      data => expect(data.stats.most_used_terms[0]._id).toEqual('aWord'),
      fail
    );
    expect(httpClientSpy.post.calls.count()).toBe(1, 'one call');
  }));
});
