import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AddSourcesService {

  configData;
  customSources;
  edited;
  currentIndex;

  constructor(
    private http: HttpClient,
    private configService: ConfigService
  ) {
    this.configData = configService.getConfig();
    this.customSources = [];
  }

  delete(body) {
    return this.http.delete(this.configData.deleteSourcesUrl, body);
  }

  modify(body) {
    if (this.edited) {
      return this.http.patch(this.configData.addSourcesUrl, body);
    } else {
      return this.http.post(this.configData.addSourcesUrl, body);
    }
  }
}
