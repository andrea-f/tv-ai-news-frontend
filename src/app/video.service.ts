import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config.service';
import { Video } from './video';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VideoService {

  configData;

  constructor(
    private http: HttpClient,
    private configService: ConfigService
  ) {
    this.configData = configService.getConfig();
  }

  vote(video: Video) {
    const body = {
      votes: video.votes,
      video_id: video.video_id
    };
    return this.http.post(this.configData.voteUrl, body);
  }

  getLatestNews(key): Observable<any> {
    // baseUrl /?source=cnn&country=uk
    // baseUrl /?country=uk
    // baseUrl /?source=cnn&
    let urlParams = '';
    if (key !== undefined) {
      urlParams = (key.source ? 'source=' + key.source + '&' : '') + (key.country ? 'country=' + key.country : '');
    }
    const fullUrl = this.configData.baseUrl + (urlParams.length > 0 ? '/?' + urlParams : '');
    // console.log('Fetching: '+ fullUrl);
    return this.http.get(fullUrl);
  }
}
