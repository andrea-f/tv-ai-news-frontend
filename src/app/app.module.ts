import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { VideoPlayerComponent } from './video-player/video-player.component';
import { HttpClientModule } from '@angular/common/http';
import { Video } from './video';
import { YoutubePlayerModule } from 'ngx-youtube-player';
import { SourcesComponent } from './sources/sources.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfigService } from './config.service';
import { SourcesService } from './sources.service';
import { VideoDataComponent } from './video-data/video-data.component';
import { AddSourcesComponent } from './add-sources/add-sources.component';
import { AddSourcesDetailComponent } from './add-sources-detail/add-sources-detail.component';
import { CookieService } from 'ngx-cookie-service';
import { RouterModule, Routes } from '@angular/router';
import { CommentsComponent } from './comments/comments.component';
import { TrendingComponent } from './trending/trending.component';
import { SourcesNavComponent } from './sources-nav/sources-nav.component';
import { MiddleLayerComponent } from './middle-layer/middle-layer.component';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: 'live',
    pathMatch: 'full'
  },
  {
    path: 'live/:source/news',
    component: MiddleLayerComponent
  },
  {
    path: 'live',
    children: [
      {
        path: 'comment',
        component: CommentsComponent
      },
      {
        path: 'sources',
        component: SourcesComponent
      }
    ]
  }
];

export function configServiceFactory(config: ConfigService) {
  return () => config.load();
}

@NgModule({
  declarations: [
    AppComponent,
    VideoPlayerComponent,
    SourcesComponent,
    VideoDataComponent,
    AddSourcesComponent,
    AddSourcesDetailComponent,
    CommentsComponent,
    TrendingComponent,
    SourcesNavComponent,
    MiddleLayerComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    HttpClientModule,
    YoutubePlayerModule,
    RouterModule.forRoot(
    appRoutes,
    {
      enableTracing: false,
    })
  ],
  providers: [
    ConfigService,
    CookieService,
    { provide: APP_INITIALIZER, useFactory: configServiceFactory, deps: [ConfigService], multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
