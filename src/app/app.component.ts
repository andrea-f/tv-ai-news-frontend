import { Injectable, Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Video } from './video';
import { VideoPlayerComponent } from './video-player/video-player.component';
import { VideoService } from './video.service';
import { CommentsComponent } from './comments/comments.component';
import { CommentsService } from './comments.service';
import { Router, RoutesRecognized } from '@angular/router';
import { TrendingService } from './trending.service';
import { SourcesService } from './sources.service';
import { SourcesNavComponent } from './sources-nav/sources-nav.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit, OnDestroy {
  title = 'NEWS TV';
  newsUrl = '';
  videosList = [];
  index = 0;
  configData;
  fullNews = [];
  fullNewsText = '';
  latestNews;
  params;
  currentVideo: Video;

  @ViewChild(VideoPlayerComponent) videoPlayer: VideoPlayerComponent;
  @ViewChild(CommentsComponent) comments: CommentsComponent;

  constructor(
    private videoService: VideoService,
    public commentsService: CommentsService,
    public trendingService: TrendingService,
    public sourcesService: SourcesService,
    public router: Router
    ) {
  }

  ngOnInit() {
    this.params = this.router.events.subscribe(event => {
      if ((event instanceof RoutesRecognized)) {
        const data = event.state.root.firstChild.params;
        if (this.trendingService) {
          // Save url preferences for trending service
          this.trendingService.data = data;
        }
        // Pass the parameters regardless if they exists or not.
        this.fetchData(data);
      }
    });
  }

  fetchData(key: object) {
    // Fetch config urls from hardcoded file from assets
    this.latestNews = this.videoService.getLatestNews(key).subscribe(data => {
      // Load the retrieved video news in an array
      this.videosList = data.news.map((video: Video) => new Video(video));
      this.videosList.forEach((video) => {
        const newsPiece = `${video.source}: ${video.description}`;
        if (this.fullNews.indexOf(newsPiece) === -1) {
          this.fullNews.push(newsPiece);
        }
        this.fullNewsText = this.fullNews.join(' - ');
      });
      if (!this.videosList[this.index]) {
        // Resets the video index to the first video in the list.
        this.index = 0;
      }
      // Find first available videos from the news
      this.currentVideo = this.videosList[this.index];
      // Set comments video id
      if (this.commentsService) {
        this.commentsService.videoId = this.currentVideo.video_id;
      }
      // Update the current video and load it only if source is specified:
      if (this.videoPlayer && key.hasOwnProperty('source')) {
        this.videoPlayer.player.loadVideoById(this.currentVideo.video_id);
      }
    });
  }

  nextVideo() {
    this.index += 1;
    // Update indexes
    this.currentVideo = this.videosList[this.index];
    if (this.currentVideo) {
      this.commentsService.videoId = this.currentVideo.video_id;
      // load and play the video directly without rerendering the player
      this.videoPlayer.player.loadVideoById(this.currentVideo.video_id);
    } else {
      // If there aren't any more videos fetch the general result.
      this.router.navigateByUrl('live');
    }
  }


  ngOnDestroy() {
    if (this.latestNews) {
      this.latestNews.unsubscribe();
    }
    if (this.params) {
      this.params.unsubscribe();
    }
  }
}
