import { async, ComponentFixture, TestBed} from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddSourcesDetailComponent } from '../add-sources-detail/add-sources-detail.component';
import { AddSourcesComponent } from './add-sources.component';
import { AddSourcesService } from '../add-sources.service';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config.service';
import { Source } from '../source';
import { CookieService } from 'ngx-cookie-service';
import { RouterTestingModule } from '@angular/router/testing';

let httpClientSpy: { get: jasmine.Spy, put: jasmine.Spy };

describe('AddSourcesComponent', () => {
  let component: AddSourcesComponent;
  let fixture: ComponentFixture<AddSourcesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSourcesComponent, AddSourcesDetailComponent ],
      imports: [FormsModule, RouterTestingModule],
      providers: [
        AddSourcesService,
        {
          provide: HttpClient, useValue: httpClientSpy
        },
        ConfigService,
        CookieService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSourcesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show sources', () => {
    const source = new Source({
      'name': 'test 123',
      'country': 'test456'
    });
    component.customSources = [source];
    fixture.detectChanges();
    const element = fixture.nativeElement;
    expect(element.querySelector('button')).toBeTruthy();
  });
});

