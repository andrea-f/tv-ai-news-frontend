import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { AddSourcesDetailComponent } from '../add-sources-detail/add-sources-detail.component';
import { Source } from '../source';
import { AddSourcesService } from '../add-sources.service';
import { RouterModule, Routes, ActivatedRoute, Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-add-sources',
  templateUrl: './add-sources.component.html',
  styleUrls: ['./add-sources.component.css']
})
export class AddSourcesComponent implements OnInit, OnDestroy {

  @ViewChild(AddSourcesDetailComponent) sourceData: AddSourcesDetailComponent;

  customSources: Array<Source>;
  currentIndex: number;
  edited: boolean;
  addSourceDelete;

  constructor(
    private addSourceService: AddSourcesService,
    private router: Router,
    private route: ActivatedRoute,
    ) {
    if (!Array.isArray(this.addSourceService.customSources)) {
      this.addSourceService.customSources = [];
    }
    this.addSourceService.edited = false;
  }

  ngOnInit() {
    this.customSources = this.addSourceService.customSources;
  }

  edit(selected) {
    const source = this.addSourceService.customSources[selected];
    this.addSourceService.currentIndex = selected;
    this.addSourceService.edited = true;
    this.router.navigate(['add-source/${selected}'], selected);
  }

  delete(selected) {
    this.addSourceService.customSources.splice(selected, 1);
    this.addSourceDelete = this.addSourceService.delete({
      sourceId: selected
    }).subscribe(data => {
      // Load the DELETE response from the server
      this.customSources = this.addSourceService.customSources;
    });
  }

  ngOnDestroy() {
    if (typeof this.addSourceDelete !== 'undefined') {
        this.addSourceDelete.unsubscribe();
    }
    this.addSourceService.customSources = this.customSources;
  }

}
