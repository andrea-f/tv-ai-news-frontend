import { Component, OnInit, OnDestroy} from '@angular/core';
import { SourcesService } from '../sources.service';
import { Source } from '../source';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-sources-nav',
  templateUrl: './sources-nav.component.html',
  styleUrls: ['./sources-nav.component.css']
})
export class SourcesNavComponent implements OnInit, OnDestroy {

  sourcesServiceGet;
  sourcesNameList: Array<String>;

  constructor(
    private sourcesService: SourcesService,
    public router: Router
  ) { }

  ngOnInit() {
    this.sourcesServiceGet = this.sourcesService.getSources().subscribe(data => {
      this.sourcesNameList = data.sources.map((source: Source) => source._id);
      // Set the service
      this.sourcesService.data = {
          sourcesNameList: this.sourcesNameList,
          // Load the retrieved sources in an array
          sources: data.sources.map((source: Source) => new Source(source))
      };
    });
  }

  ngOnDestroy() {
    this.sourcesServiceGet.unsubscribe();
  }

  changeSource(param: string) {
    param = decodeURIComponent(param);
    this.router.navigateByUrl('live/' + param + '/news');
  }

}
