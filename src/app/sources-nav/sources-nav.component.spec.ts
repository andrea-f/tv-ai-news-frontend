import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SourcesNavComponent } from './sources-nav.component';

describe('SourcesNavComponent', () => {
  let component: SourcesNavComponent;
  let fixture: ComponentFixture<SourcesNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SourcesNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SourcesNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
