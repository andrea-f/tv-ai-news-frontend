import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SourcesComponent } from './sources.component';
import { Video } from '../video';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { of } from 'rxjs';
import { SourcesService } from '../sources.service';
import { CookieService } from 'ngx-cookie-service';

let httpClientSpy: { get: jasmine.Spy, put: jasmine.Spy };
let configService: ConfigService;
let sourcesService: SourcesService;

describe('SourcesComponent', () => {
  let component: SourcesComponent;
  let fixture: ComponentFixture<SourcesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SourcesComponent ],
      providers: [
        ConfigService,
        SourcesService,
        CookieService,
        {provide: HttpClient, useValue: httpClientSpy}
      ],
      imports: [
        FormsModule,
        ReactiveFormsModule
      ]
    })
    .compileComponents();
    configService = TestBed.get(ConfigService);
    sourcesService = TestBed.get(SourcesService);

    spyOn(configService, 'getConfig').and.returnValue({
      'sourcesUrl': 'empty',
      'baseUrl': 'empty'});
    spyOn(sourcesService, 'getSources').and.returnValue(of({
      'sources': []
    }));

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SourcesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
