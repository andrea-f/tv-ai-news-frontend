import { Component, OnInit, Input, OnDestroy} from '@angular/core';
import { Source } from '../source';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { NgForm } from '@angular/forms';
import { SourcesService } from '../sources.service';

@Component({
  selector: 'app-sources',
  templateUrl: './sources.component.html',
  styleUrls: ['./sources.component.css']
})
export class SourcesComponent implements OnInit, OnDestroy {

  sourcesList: Array<Source>;
  // Since source is not yet a separate db entity
  // Create a copy and send the change from what the name was to use as a key in mongo
  // and what the user changed it to:
  sourcesNameList: Array<String>;
  edited: Boolean;
  sourcesServiceGet;
  sourcesServiceEdit;

  constructor(
    private sourcesService: SourcesService
    ) {
      this.sourcesNameList = [];
    }

  ngOnInit() {
    if (!this.sourcesService.data) {
      this.sourcesServiceGet = this.sourcesService.getSources().subscribe(data => {
        // Load the retrieved sources in an array
        this.sourcesList = data.sources.map((source: Source) => new Source(source));
        // Set source names for reference
        this.updateSourceNames(this.sourcesList);
        this.sourcesService.data = {
          sourcesNameList: this.sourcesNameList,
          sources: this.sourcesList
        };
      });
    } else {
      this.sourcesNameList = this.sourcesService.data.sourcesNameList;
      this.sourcesList = this.sourcesService.data.sources;
    }
  }

  updateSourceNames(sourcesList: Array<Source>) {
    for (const source of sourcesList) {
      this.sourcesNameList.push(source._id);
    }
  }

  onSubmit(submittedForm: NgForm) {
    const
      sourceId = submittedForm.value.index,
      oldName = this.sourcesNameList[sourceId],
      newName = submittedForm.value.name;
    this.edited = true;
    this.sourcesServiceEdit = this.sourcesService.editSource({
      'current': oldName,
      'new': newName
    }).subscribe(data => {
      this.sourcesList = data.sources.map((source: Source) => new Source(source));
      this.updateSourceNames(this.sourcesList);
      this.edited = false;
    });
  }

  ngOnDestroy() {
    if (this.edited) {
      this.sourcesServiceEdit.unsubscribe();
    }
    if (this.sourcesServiceGet) {
      this.sourcesServiceGet.unsubscribe();
    }
  }
}
