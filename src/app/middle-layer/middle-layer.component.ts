import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-middle-layer',
  templateUrl: './middle-layer.component.html',
  styleUrls: ['./middle-layer.component.css']
})
export class MiddleLayerComponent implements OnInit {

  constructor(
  ) { }

  ngOnInit() {
    // TODO: News source dashboard will be populated here
  }

}
