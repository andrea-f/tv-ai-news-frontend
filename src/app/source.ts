export class Source {
  country = '';
  name  = '';
  total_articles = 0;
  _id   = '';
  url   = '';
  rules = '';
  count = '';
  stats = {};

  constructor (source) {
    this.country = source.country;
    this.name = source.name;
    this.total_articles = source.total_articles;
    this._id = source._id;
    this.url = source.url;
    this.rules = source.rules;
    this.count = source.count;
    this.stats = source.stats;
  }
}
