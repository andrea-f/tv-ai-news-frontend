import { Component, OnDestroy } from '@angular/core';
import { FormGroup, FormControl} from '@angular/forms';
import { NgForm } from '@angular/forms';
import { CommentsService } from '../comments.service';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css'],
  providers: [NgForm]
})
export class CommentsComponent implements OnDestroy {

  addCommentService;
  currentVideo;
  videoId;

  constructor(
    public form: NgForm,
    private commentsService: CommentsService
  ) { }

  addComment(data) {
    const body = {
      comment: data.comment,
      videoId: (this.videoId ? this.videoId : 'NoVideoId'),
      email: data.email
    };
    console.log(body);
    this.addCommentService = this.commentsService.add(body).subscribe(dataResponse => {});
  }

  onSubmit(form: NgForm) {
    // Set the video id when the user submits the form
    if (this.commentsService.videoId !== undefined) {
      this.videoId = this.commentsService.videoId;
    }
    if (form.valid) {
      this.form = form;
      this.addComment(form.value);
      this.form.reset();
      form.reset();
    }
  }

  ngOnDestroy() {
    if (this.addCommentService !== undefined) {
      this.addCommentService.unsubscribe();
    }
  }

}
