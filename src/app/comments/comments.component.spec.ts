import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { CommentsComponent } from './comments.component';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config.service';
import { CookieService } from 'ngx-cookie-service';

let httpClientSpy: { get: jasmine.Spy, put: jasmine.Spy };

describe('CommentsComponent', () => {
  let component: CommentsComponent;
  let fixture: ComponentFixture<CommentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommentsComponent ],
      providers: [
        ConfigService,
        CookieService,
        {provide: HttpClient, useValue: httpClientSpy}
      ]
      imports: [FormsModule]
    })
    .compileComponents();
  }));

  it('should create', () => {
    fixture = TestBed.createComponent(CommentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

});
