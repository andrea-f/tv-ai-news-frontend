import { Component, OnInit, EventEmitter, Output, Input} from '@angular/core';
import { Source } from '../source';
import { FormGroup, FormControl} from '@angular/forms';
import { NgForm } from '@angular/forms';
import {  RouterModule, Routes, ActivatedRoute, Router } from '@angular/router';
import { AddSourcesService } from '../add-sources.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-add-sources-detail',
  templateUrl: './add-sources-detail.component.html',
  styleUrls: ['./add-sources-detail.component.css'],
  providers: [NgForm]
})
export class AddSourcesDetailComponent implements OnInit {

  @Input() source: Source;
  @Output() formEvent = new EventEmitter();
  addSourceModify;

  constructor(
    public form: NgForm,
    private router: Router,
    private addSourceService: AddSourcesService,
    private route: ActivatedRoute
    ) {
  }

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id');
    if (typeof this.addSourceService.customSources !== 'undefined' &&
     this.addSourceService.customSources.length > 0 &&
     this.addSourceService.edited === true) {
      this.source = this.addSourceService.customSources[id];
    } else {
      this.source = new Source({
        'name': '',
        'country': '',
        'url': '',
        'rules': ''
      });
    }
  }

  addSource(data) {
    if (this.addSourceService.currentIndex !== undefined) {
      this.addSourceService.customSources[this.addSourceService.currentIndex] = data;
      this.addSourceService.edited = true;
      this.addSourceService.currentIndex = undefined;
    } else {
      this.addSourceService.customSources.push(data);
    }
    this.addSourceModify = this.addSourceService.modify({
      source: data,
      sourceId: this.addSourceService.currentIndex
    }).subscribe(dataResponse => {
      // Load the retrieved sources in an array
      this.router.navigate(['add-sources']);
    });
  }

  onSubmit(form: NgForm) {
    if (form.valid) {
      this.form = form;
      this.addSource(form.value);
      form.reset();
      this.form.reset();
    }
  }

  ngOnDestry() {
    if (typeof this.addSourceModify !== 'undefined') {
      this.addSourceModify.unsubscribe();
    }
  }

}
