import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddSourcesDetailComponent } from './add-sources-detail.component';
import { Source } from '../source';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config.service';
import { CookieService } from 'ngx-cookie-service';

let httpClientSpy: { get: jasmine.Spy };

describe('AddSourcesDetailComponent', () => {
  let component: AddSourcesDetailComponent;
  let fixture: ComponentFixture<AddSourcesDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSourcesDetailComponent ],
      providers: [
        {provide: HttpClient, useValue: httpClientSpy},
        ConfigService,
        CookieService
      ],
      imports: [FormsModule, RouterTestingModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSourcesDetailComponent);
    component = fixture.componentInstance;
    component.source = new Source({
      'name': 'test 123',
      'country': 'test456'
    });
    fixture.detectChanges();
  });

  it('should create FORM', () => {
    expect(component).toBeTruthy();
  });

  xit('FORM doesnt validate if empty', () => {
    component.form.onSubmit(new Event('click'));
    expect(component.form.valid).toBeFalsy();
  });

  xit('name field validity', () => {
    component.source = new Source({name: 'x', url: 'z', country: 'q', rules: 'r'});
    expect(component.form.value.name).toBeFalsy();
  });

  xit('country field validity', () => {
    component.source = new Source({name: 'x', url: 'z', country: 'q', rules: 'r'});
    expect(component.form.value.country.valid).toBeFalsy();
  });

  xit('submit FORM with data', () => {
    component.source = new Source({name: 'x', url: 'z', country: 'q', rules: 'r'});
    component.form.value.country = component.source.country;
    component.form.value.name = component.source.name;
    expect(component.form.form.valid).toBeTruthy();
  });
});
