import { TestBed, inject } from '@angular/core/testing';
import { defer } from 'rxjs';
import { ConfigService } from './config.service';
import { HttpClient } from '@angular/common/http';
import { AddSourcesService } from './add-sources.service';
import { CookieService } from 'ngx-cookie-service';

let httpClientSpy: { post: jasmine.Spy, patch: jasmine.Spy };
let configService: ConfigService;

export function asyncData<T>(data: T) {
  return defer(() => Promise.resolve(data));
}

describe('AddSourcesService', () => {
  beforeEach(() => {

    httpClientSpy = jasmine.createSpyObj('HttpClient', ['post', 'patch']);

    TestBed.configureTestingModule({
      providers: [
      ConfigService,
      {provide: HttpClient, useValue: httpClientSpy},
      CookieService,
      ]
    });
    configService = TestBed.get(ConfigService);
    spyOn(configService, 'getConfig').and.returnValue({'addSourcesUrl': 'empty'});
  });

  it('should be created', inject([AddSourcesService], (service: AddSourcesService) => {
    expect(service).toBeTruthy();
  }));

  it('should do a PATCH request to modify a source', inject([AddSourcesService], (service: AddSourcesService) => {
    const expectedData: object = {edited: true};
    service.edited = true;
    httpClientSpy.patch.and.returnValue(asyncData(expectedData));

    service.modify(expectedData).subscribe(
      data => expect(data).toBeTruthy(),
      fail
    );
    expect(httpClientSpy.patch.calls.count()).toBe(1, 'one call');
  }));

  it('should do a POST request to add a source', inject([AddSourcesService], (service: AddSourcesService) => {
    const expectedData: object = {};
    httpClientSpy.post.and.returnValue(asyncData(expectedData));
    service.modify(expectedData).subscribe(
      data => expect(data).toBeTruthy(),
      fail
    );
    expect(httpClientSpy.post.calls.count()).toBe(1, 'one call');
  }));


});
