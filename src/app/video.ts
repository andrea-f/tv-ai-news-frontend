export class Video {
  video_id = '';
  data =  {};
  country =  '';
  date = ''; // '1531120022096'
  description = '';
  source = '';
  time = ''; // '1531038441',
  title = '';
  votes = []; // [{vote:0,user_id: '123456'}], 0 thumbs down, 1 thumbs up
  url = ''; // 'http://c.newsnow.co.uk/A/945394446?-14432:11:3',

  constructor(private video) {
    this.video_id = video.video_id;
    this.country = video.country;
    this.date = video.date;
    this.description = video.description;
    this.source = video.source;
    this.time = video.time;
    this.title = video.title;
    this.data = video.data;
    this.votes = video.votes;
    this.url = video.url;
  }
}
