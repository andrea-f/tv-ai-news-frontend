import { TestBed, inject } from '@angular/core/testing';
import { defer } from 'rxjs';
import { ConfigService } from './config.service';
import { HttpClient } from '@angular/common/http';
import { CommentsService } from './comments.service';
import { CookieService } from 'ngx-cookie-service';

let httpClientSpy: { post: jasmine.Spy };
let configService: ConfigService;

export function asyncData<T>(data: T) {
  return defer(() => Promise.resolve(data));
}

describe('CommentsService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['post']);

    TestBed.configureTestingModule({
      providers: [
        ConfigService,
        {provide: HttpClient, useValue: httpClientSpy},
        CommentsService,
        CookieService
      ]
    });
    configService = TestBed.get(ConfigService);
    spyOn(configService, 'getConfig').and.returnValue({'addCommentUrl': 'empty'});
  });

  it('should be created', inject([CommentsService], (service: CommentsService) => {
    expect(service).toBeTruthy();
  }));

  it('should send a POST with comment data and video id', inject([CommentsService], (service: CommentsService) => {
    const expectedData: object = {
      email: 'email@email.it',
      comment: 'xyz',
      video_id: 'abcd1234'
    };
    httpClientSpy.post.and.returnValue(asyncData(expectedData));
    service.add(expectedData).subscribe(
      data => expect(data).toBeTruthy(),
      fail
    );
    expect(httpClientSpy.post.calls.count()).toBe(1, 'one call');
  }));

});
