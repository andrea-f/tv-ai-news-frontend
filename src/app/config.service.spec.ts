import { TestBed, inject } from '@angular/core/testing';
import { defer } from 'rxjs';
import { ConfigService } from './config.service';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';

let httpClientSpy: { get: jasmine.Spy };
let configService: ConfigService;
let cookieService: CookieService;

export function asyncData<T>(data: T) {
  return defer(() => Promise.resolve(data));
}

describe('ConfigService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    TestBed.configureTestingModule({
      providers: [
      ConfigService,
      CookieService,
      {provide: HttpClient, useValue: httpClientSpy}
      ]
    });
    configService = new ConfigService(<any> httpClientSpy, <any> configService);
  });

  it('should be created', inject([ConfigService], (service: ConfigService) => {
    expect(service).toBeTruthy();
  }));

  it('should load JSON config file', inject([ConfigService], (service: ConfigService) => {
    const expectedData = {};
    httpClientSpy.get.and.returnValue(asyncData(expectedData));
    configService.load().then(
      data => expect(data).toBe(expectedData),
      fail
    );
    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  }));
});
